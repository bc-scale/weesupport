![Imgur](https://i.imgur.com/ztYDqR0.png)
# WeeSupport

A simple-ish bot to assist with the organisation of your Discord community's support channels.

### Why?
A lot of my most recent contributions have been to the [Arcadia Technology / Crafty Controller](https://gitlab.com/crafty-controller/crafty-4) project.

During my time contributing and supporting users in their Discord there was always hurdles with support, users posting in the wrong channel, users not posting issues in threads or threads not being tracked correctly (resolved/unresolved).

WeeSupport was created to resolve these issues and provide a clear and concise support process.

## Features
- Makes use of modern Discord.js features such as **(/) Commands**, **Modal Inputs** & **Threads**.
- Minimal setup, select your support channels, moderator roles and away you go!
- Directs users (by DM) to support channels if command is used in a non-support channel.
- Automated process of ticket creation and resolution. Renaming, locking and archiving threads on resolution.
- Deletes any message in a support channel that is not a **(/) command**, and removes '*started a thread*' message on resolution for easy open issue tracking. (Mod messages are exempt)


## Screenshots
Below are demonstrations of WeeSupport functionality in action on Crafty Controller's Deployment:

#### Ticket Creation:
![(/) Command](https://i.imgur.com/Q1Wzfmg.png)
![Input Form](https://i.imgur.com/hnP9P2S.png)![Ticket Control](https://i.imgur.com/blB7y9I.png)

#### Resolved Ticket:
![Resolved Ticket](https://i.imgur.com/eRZimDO.png)![Archived Tickets](https://i.imgur.com/t2L8v1C.png)


## Config
Found in [./config/config.js](/config/config.js)
```js
export default {
	token: '<Discord-token>',

		supportChannels: [
		{
			channelId: '<channelId>',
			initialMessage: '<can be blank>',
		},
	],
	moderatorRoles: ['<roleId>', '<roleId>', '<roleId>'],
};

```
## Deployment
- Start off by cloning down the repository applying your config with the above example.
    ```bash
    $ git clone git@gitlab.com:Zedifus/weesupport.git
    $ cd weesupport
    $ vim config/config.js
    ```

- Then deploy your **(/) commands** by copying your bot's **client-id** & your guild's **guild-id** into the designated variables on **line 11 & 12** of [exportCommands.js](/src/discord/exportCommands.js). Then run the following commands to deploy **(/) commands** and start bot!
    ```bash
    $ npm run cmdExport # To dispatch (/) commands
    $ npm start # To start bot
    ```

>I have also included a `docker-compose` config. Just simply clone like above, configure and build
```bash
$ docker-compose up -d && docker-compose logs -f
```


## Acknowledgements
 - [TomboFry](https://gitlab.com/TomboFry) - For the lightweight logger lib

## Roadmap

- **Make almost every string/emoji configurable in strings.js**

- **Ticket question neutrality / easily configurable form questions** <br>
  The support ticket form/modal is very *Crafty Controller* centric at the moment, to deploy for your own usage would require significant editing of [supportHandler.js](/src/discord/supportHandler.js), my plan is to make it so that issue questions are easily configurable, like you would customise strings in the [string.js config](/config/strings.js).

- **Easier (/) command deployment**
  Deployment of slash commands is trickier than i'd like because you need to edit variables in [exportCommands.js](/src/discord/exportCommands.js), in the future I'll make this an option in **config.js**


## Contributing

Contributions are always welcome!

I am always seeking to learn and improve, so if you have ideas, feel free to open a MR!
## Credit / Donations
I don't want your money, or mind wither you rebrand my code in your projects if you
happen to use it, if it's helped you or you have deployed it. All I ask is please
just pop a wee star on this repository, and adhere to the below licence.

It'd mean the world to me 🥰
## License

[GPL-V3.0](https://choosealicense.com/licenses/gpl-3.0/)

