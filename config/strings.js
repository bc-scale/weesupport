// UI
export const uiModalTitleCreate = 'Create a Support Ticket!';
export const uiModalTitleEdit = 'Edit a Support Ticket!';

// Error messages
export const errorCommandInteraction = 'There was an error while executing this command! 😭';
