import { SlashCommandBuilder } from 'discord.js';
import { initSupport } from '../supportHandler.js';

const command = {
	data: new SlashCommandBuilder()
		.setName('support')
		.setDescription('Request Support! - Opens a ticket!'),
	async execute (interaction) {
		await initSupport(interaction);
	},
};

export default command;
