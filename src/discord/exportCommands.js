/* eslint-disable no-console */
import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';
import fs from 'fs';
import config from '../../config/config.js';

const commands = [];
const commandFiles = fs.readdirSync('./src/discord/commands').filter(file => file.endsWith('.js'));

// Place your client and guild ids here (GuildId for dev, for prod leave blank)
const clientId = '';
const guildId = '';
// TODO Move to config.js

async function gatherCommands () {
	for (const file of commandFiles) {
		const command = (await import(`./commands/${file}`)).default;

		commands.push(command.data.toJSON());
	}
}

const rest = new REST({ version: '9' }).setToken(config.token);

(async () => {
	// Bail if not configured
	if (!clientId || !guildId){
		console.error("Please configure (/) command exporter in '/src/discord/exportCommands.js'");
		console.warn('Exiting...');
		process.exitCode = 1;

		return;
	}

	await gatherCommands();
	console.log(commands);

	try {
		console.log('Started refreshing application (/) commands.');

		await rest.put(
			Routes.applicationGuildCommands(clientId, guildId),
			{ body: commands },
		);

		console.log('Successfully reloaded application (/) commands.');
	} catch (error) {
		console.error(error);
	}
})();
