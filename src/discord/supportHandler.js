import {
	ActionRowBuilder,
	ModalBuilder,
	TextInputBuilder,
	TextInputStyle,
	EmbedBuilder,
	ButtonBuilder,
	ButtonStyle,
	ChannelType,
	PermissionsBitField,
	InteractionType,
} from 'discord.js';

import Logger from '../lib/logger.js';
const logger = new Logger('SUPPORT');

import * as strings from '../../config/strings.js';
import config from '../../config/config.js';

const linkableChannels = config.supportChannels.map(chanObj => { return `<#${chanObj.channelId}>`; });

export async function initSupport (commandInteraction) {
	const { guild, user, channel } = commandInteraction;

	// Validate origin of (/) command.
	// If interaction comes from anywhere apart from a 'text' support channel then bail
	// (Stops silly people trying to thread in threads etc)
	if (
		channel.type !== ChannelType.GuildText ||
		!config.supportChannels.some(chanObj => chanObj.channelId === channel.id)
	) {
		await commandInteraction.reply({
			content: `This command only works in a designated support channel (${linkableChannels.join(', ')})`,
			ephemeral: true,
		});
		return;
	}

	logger.info(
		`\x1B[34mSupport ticket initiated in '\x1B[32m${guild} \x1B[34m | `,
		`\x1B[32m${channel.id}\x1B[34m' for \x1B[32m${user.username}(${user.id}) \x1b[0m`,
	);

	// Prepare
	const modal = new ModalBuilder()
		.setCustomId('ticketCreate')
		.setTitle(strings.uiModalTitleCreate);

	// Create the text input components
	const titleInput = new TextInputBuilder()
		.setCustomId('ticketTitle')
		.setLabel('Ticket Title:')
		.setMaxLength(90)
		.setStyle(TextInputStyle.Short)
		.setRequired(true);

	const osInput = new TextInputBuilder()
		.setCustomId('osInput')
		.setLabel('Host Operating System:')
		.setMaxLength(45)
		.setStyle(TextInputStyle.Short)
		.setPlaceholder('e.g. Linux(distro), Windows, Mac, Docker')
		.setRequired(true);

	const typeInput = new TextInputBuilder()
		.setCustomId('typeInput')
		.setLabel('Server Type:')
		.setMaxLength(45)
		.setStyle(TextInputStyle.Short)
		.setPlaceholder('e.g. minecraft-java, minecraft-bedrock, N/A')
		.setRequired(true);

	const versionInput = new TextInputBuilder()
		.setCustomId('versionInput')
		.setLabel('Server Version:')
		.setMaxLength(45)
		.setStyle(TextInputStyle.Short)
		.setPlaceholder('e.g. Forge, Paper, Spigot, N/A')
		.setRequired(true);

	const descriptionInput = new TextInputBuilder()
		.setCustomId('descriptionInput')
		.setLabel('Ticket Body:')
		.setStyle(TextInputStyle.Paragraph)
		.setPlaceholder('Please describe the issue you are having.')
		.setMinLength(20)
		.setMaxLength(2000)
		.setRequired(true);

	// An action row only holds one text input,
	const firstActionRow = new ActionRowBuilder().addComponents(titleInput);
	const secondActionRow = new ActionRowBuilder().addComponents(osInput);
	const thirdActionRow = new ActionRowBuilder().addComponents(typeInput);
	const fourthActionRow = new ActionRowBuilder().addComponents(versionInput);
	const fifthActionRow = new ActionRowBuilder().addComponents(descriptionInput);

	// Add inputs to the modal
	modal.addComponents(
		firstActionRow,
		secondActionRow,
		thirdActionRow,
		fourthActionRow,
		fifthActionRow,
	);

	// Display Modal form
	try {
		await commandInteraction.showModal(modal);
	} catch (e) {
		logger.error(e);
	}
}

export async function ticketReceiver (modalInteraction){
	const { guild, user, member, fields, channel } = modalInteraction;

	// To be nice lets call them by their vanity, but handle if they don't have one!
	const endUser = member.nickname ? member.nickname : user.username;
	const endUserAvatar = member.avatarURL() ? member.avatarURL() : user.avatarURL();

	const thread = await modalInteraction.channel.threads.create({
		name: fields.getTextInputValue('ticketTitle'),
		autoArchiveDuration: 1440, // 1 day
		reason: `Support Ticket System [UserID: ${user.username}(${user.id})]`,
	});

	const embedMain = new EmbedBuilder()
		.setAuthor({ name: `${guild.name} Support`, iconURL: await guild.iconURL() })
		.setColor('#00ffd6')
		.setDescription(`**Issue Description**: \n${fields.getTextInputValue('descriptionInput')}`)
		.setFields(
			{
				name: 'Host Operating System:',
				value: fields.getTextInputValue('osInput'),
				inline: false,
			},
			{
				name: 'Server Type:',
				value: fields.getTextInputValue('typeInput'),
				inline: false,
			},
			{
				name: 'Server Version:',
				value: fields.getTextInputValue('versionInput'),
				inline: false,
			},
			{
				name: `Ticket Creator: ${endUser}`,
				value: user.id,
			},
		)
		.setFooter({
			text: `Submitted by: ${endUser}`,
			iconURL: endUserAvatar,
		});

	const submitButton = new ButtonBuilder()
		.setEmoji('✅')
		.setCustomId('ticketResolved')
		.setLabel('Resolve')
		.setStyle(ButtonStyle.Success);

	const editButton = new ButtonBuilder()
		.setEmoji('📝')
		.setCustomId('ticketEdit')
		.setLabel('Edit')
		.setStyle(ButtonStyle.Primary);

	const wikiLink = new ButtonBuilder()
		.setURL('https://wiki.craftycontrol.com/')
		.setStyle(ButtonStyle.Link)
		.setLabel('Wiki');

	const embedActionRow = new ActionRowBuilder()
		.addComponents(submitButton)
		.addComponents(editButton)
		.addComponents(wikiLink);

	// Post ticket content, and pin to thread
	const ticketEmbed = await thread.send({ embeds: [ embedMain ], components: [ embedActionRow ] });
	await ticketEmbed.pin();

	// Add ticket creator
	await thread.members.add(user.id);

	// Tidy up prep notifications (e.g. bot pinned msg)
	try {
		const fetched = await thread.messages.fetch();
		const notPinned = fetched.filter(msg => !msg.pinned);

		await thread.bulkDelete(notPinned, true);
	} catch (err) {
		logger.error('Error during post setup cleanup:', err);
	}

	// Add thread creation message after thread processing (If provided)
	try {
		const chanObj = config.supportChannels.find(chanObj => chanObj.channelId === channel.id);
		if (chanObj.initialMessage) await thread.send(chanObj.initialMessage);
	} catch (err) {
		logger.error('Error posting initial message in thread:', err);
	}

	// Return a reaction to the origin interaction so discord knows we handled it (Like 200 OK)
	await modalInteraction.reply({
		content: 'Your ticket was processed successfully!',
		ephemeral: true,
	});

	logger.info(`\x1B[34mCreated Support thread: \x1B[32m${thread.name}\x1b[0m`);
}

export async function resolveTicket (buttonInteraction) {
	const { channel, user, member, message } = buttonInteraction;
	const threadNameOrig = channel.name;
	const embedOrigin = EmbedBuilder.from(message.embeds[0]);

	// Last field 'should' always be id of creator.
	const ticketCreatorId = message.embeds[0].fields[message.embeds[0].fields.length - 1].value;

	// To be nice lets call them by their vanity, but handle if they don't have one!
	const endUser = member.nickname ? member.nickname : user.username;
	const endUserAvatar = member.avatarURL() ? member.avatarURL() : user.avatarURL();

	logger.info(
		`\x1B[34mResolving Ticket: \x1B[32m${channel.name} \n\x1B[34m[Resolved `,
		`By: \x1B[32m${user.username}(${user.id})\x1B[34m]\x1b[0m`,
	);

	// Only resolve if Admin, Staff or ticket owner, bail for anyone else.
	if (
		member.permissions.has(PermissionsBitField.Flags.Administrator) ||
		config.moderatorRoles.some(id => member.roles.cache.has(id)) ||
		user.id === ticketCreatorId
	) {
		// Remove buttons from embed
		const resolvedEmbed = new EmbedBuilder()
			.setColor('#2ECC71')
			.setFooter({
				text: `Resolved by: ${endUser} 🔒`,
				iconURL: endUserAvatar,
			});
		message.edit({ components: [], embeds: [ embedOrigin, resolvedEmbed ] });

		// Return a reaction to the origin interaction so discord knows we handled it (Like 200 OK)
		await buttonInteraction.reply({
			content: 'This thread has been resolved!',
			ephemeral: true,
		});

		// Delete parent channel message
		try {
			const originMessage = await channel.fetchStarterMessage();
			await originMessage.delete();
		} catch (err) {
			if (err.status === 404) {
				logger.warn('Thread parent already deleted');
			} else {
				logger.error(err);
			}
		}

		// Archive Thread
		const resolvedTitle = `RESOLVED ▷ ${threadNameOrig}`;

		// Thread titles are limited to 100 chars, lets prevent a future stack for long titles
		await channel.setName(resolvedTitle.length > 100
			? `${resolvedTitle.substring(0, 97)}...` : resolvedTitle);
		await channel.setLocked(true, 'Support Ticket System | Resolved');
		await channel.setArchived(true, `Support Ticket System [Resolved By: ${user.username}(${user.id})]`);

	} else {
		await buttonInteraction.reply({
			content: 'Only the ticket creator or staff can resolve.',
			ephemeral: true,
		});
		return;
	}
}

export async function editTicket (interaction) {
	const { channel, message, user, member, fields } = interaction;
	let embedOrigin = EmbedBuilder.from(message.embeds[0]);

	// Last field 'should' always be id of creator.
	const ticketCreatorId = message.embeds[0].fields[message.embeds[0].fields.length - 1].value;

	// To be nice lets call them by their vanity, but handle if they don't have one!
	const endUser = member.nickname ? member.nickname : user.username;
	const endUserAvatar = member.avatarURL() ? member.avatarURL() : user.avatarURL();

	// On receiving edit request display edit modal
	// Only proceed if Admin, Staff or ticket owner, bail for anyone else.
	if (
		member.permissions.has(PermissionsBitField.Flags.Administrator) ||
		config.moderatorRoles.some(id => member.roles.cache.has(id)) ||
		user.id === ticketCreatorId
	) {
		if (interaction.type === InteractionType.MessageComponent){
			logger.info(
				`\x1B[34mEditing Ticket: \x1B[32m${channel.name} \n\x1B[34m[Called `,
				`By: \x1B[32m${user.username}(${user.id})\x1B[34m]\x1b[0m`,
			);

			// Prepare
			const modal = new ModalBuilder()
				.setCustomId('ticketEdit')
				.setTitle(strings.uiModalTitleEdit);

			// Create the text input components
			const titleInput = new TextInputBuilder()
				.setCustomId('ticketTitle')
				.setLabel('Ticket Title:')
				.setPlaceholder(channel.name)
				.setMaxLength(90)
				.setStyle(TextInputStyle.Short)
				.setRequired(false);

			const osInput = new TextInputBuilder()
				.setCustomId('osInput')
				.setLabel('Host Operating System:')
				.setPlaceholder(message.embeds[0].fields[0].value)
				.setMaxLength(45)
				.setStyle(TextInputStyle.Short)
				.setRequired(false);

			const typeInput = new TextInputBuilder()
				.setCustomId('typeInput')
				.setLabel('Server Type:')
				.setPlaceholder(message.embeds[0].fields[1].value)
				.setMaxLength(45)
				.setStyle(TextInputStyle.Short)
				.setRequired(false);

			const versionInput = new TextInputBuilder()
				.setCustomId('versionInput')
				.setLabel('Server Version:')
				.setPlaceholder(message.embeds[0].fields[2].value)
				.setMaxLength(45)
				.setStyle(TextInputStyle.Short)
				.setRequired(false);

			// Remmove '**Issue Description**: '
			const descriptionClean = message.embeds[0].description.slice(24);

			// From what I can tell placeholders have an undocumented limit of <= 100
			const descriptionInput = new TextInputBuilder()
				.setCustomId('descriptionInput')
				.setLabel('Ticket Body:')
				.setPlaceholder(descriptionClean.length > 100
					? `${descriptionClean.substring(0, 97)}...` : descriptionClean)
				.setStyle(TextInputStyle.Paragraph)
				.setMinLength(20)
				.setMaxLength(2000)
				.setRequired(false);

			// An action row only holds one text input,
			const firstActionRow = new ActionRowBuilder().addComponents(titleInput);
			const secondActionRow = new ActionRowBuilder().addComponents(osInput);
			const thirdActionRow = new ActionRowBuilder().addComponents(typeInput);
			const fourthActionRow = new ActionRowBuilder().addComponents(versionInput);
			const fifthActionRow = new ActionRowBuilder().addComponents(descriptionInput);

			// Add inputs to the modal
			modal.addComponents(
				firstActionRow,
				secondActionRow,
				thirdActionRow,
				fourthActionRow,
				fifthActionRow,
			);

			// Display Modal form
			try {
				await interaction.showModal(modal);
			} catch (e) {
				logger.error(e);
			}
			return;
		}
	} else {
		await interaction.reply({
			content: 'Only the ticket creator or staff can edit ticket.',
			ephemeral: true,
		});
		return;
	}

	// On receiving form response prepare edits
	if (interaction.type === InteractionType.ModalSubmit){

		if (fields.getTextInputValue('ticketTitle')) {
			channel.setName(fields.getTextInputValue('ticketTitle'));
		}

		if (fields.getTextInputValue('descriptionInput')) {
			embedOrigin.setDescription(`**Issue Description**: \n${fields.getTextInputValue('descriptionInput')}`);
		}

		if (fields.getTextInputValue('osInput')) {
			embedOrigin.spliceFields(0, 1, {
				name: 'Host Operating System:',
				value: fields.getTextInputValue('osInput'),
				inline: false,
			});
		}

		if (fields.getTextInputValue('typeInput')) {
			embedOrigin.spliceFields(1, 1, {
				name: 'Server Type:',
				value: fields.getTextInputValue('typeInput'),
				inline: false,
			});
		}

		if (fields.getTextInputValue('versionInput')) {
			embedOrigin.spliceFields(2, 1, {
				name: 'Server Version:',
				value: fields.getTextInputValue('versionInput'),
				inline: false,
			});
		}

		embedOrigin.setFooter({
			text: `Edited by: ${endUser}`,
			iconURL: endUserAvatar,
		});

		// Edit the embed
		await message.edit({ embeds: [ embedOrigin ] });

		// Return a reaction to the origin interaction so discord knows we handled it (Like 200 OK)
		await interaction.reply({
			content: 'Ticket Edited',
			ephemeral: true,
		});

		logger.info(
			`\x1B[34mTicket edit success: \x1B[32m${channel.name} \n\x1B[34m[Edited `,
			`By: \x1B[32m${user.username}(${user.id})\x1B[34m]\x1b[0m`,
		);
	}
}
