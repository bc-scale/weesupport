import fs from 'fs';
import { Collection, InteractionType } from 'discord.js';


// Load the config and strings
import config from '../config/config.js';
import * as strings from '../config/strings.js';

// Bot object - MESSAGE & REACTION partials so we can handle reacts on older messages
import * as BotAdapter from './lib/botAdapter.js';
const bot = BotAdapter.getInstance();

import Logger from './lib/logger.js';
const logger = new Logger('DISCORD');

import { resolveTicket, editTicket, ticketReceiver } from './discord/supportHandler.js';
import { handleMessage } from './discord/messageHandler.js';

// Setup (/) Commands
bot.commands = new Collection();

async function gatherCommands () {
	// readdirSync uses the process's current working directory to resolve paths.
	// Typically this will be the root of the repo (as npm run *) works from there...
	const commandFiles = fs.readdirSync('./src/discord/commands').filter(file => file.endsWith('.js'));

	for (const file of commandFiles) {
		// However, import() uses the current file's directory to resolve paths...
		const command = (await import(`./discord/commands/${file}`)).default;
		logger.info(`\x1b[35mCommand found - \x1b[36m /${command.data.name} \x1b[0m`);
		// With the key as the command name and the value as the exported module
		bot.commands.set(command.data.name, command);
	}
}

(async () => {

	// Config check
	if (!config.token){
		logger.error('Token not found in config, check your config!');
		return;
	}
	if (config.supportChannels.length <= 0){
		logger.error('Support channels not defined, please supply suport channel(s)!');
		return;
	}
	if (config.supportChannels.some(chanObj => !chanObj.channelId)){
		logger.error('One of your defined support channels is misconfigured, please check config!');
		return;
	}
	if (config.moderatorRoles.length <= 0){
		logger.warn('No mod roles defined, only admins will have ticket overrides!');
	}

	// Ingest commands and add them to the bots command collection!
	await gatherCommands();

	// This event will run on every single message received, from any channel or DM.
	// If it's a bot discard so we don't delete useful messages
	bot.on('messageCreate', message => {
		if (!message.author.bot) handleMessage(message);
	});

	// Slash (/) Commands Handler
	bot.on('interactionCreate', async interaction => {

		// Handle modal form responces
		if (
			interaction.customId === 'ticketCreate' &&
            interaction.type === InteractionType.ModalSubmit
		) {
			await ticketReceiver(interaction);
			return;
		}

		// Handle button interaction responces
		if (
			interaction.customId === 'ticketResolved' &&
            interaction.type === InteractionType.MessageComponent
		) {
			await resolveTicket(interaction);
			return;
		}

		if (
			interaction.customId === 'ticketEdit' &&
            (interaction.type === InteractionType.MessageComponent ||
			interaction.type === InteractionType.ModalSubmit)
		) {
			await editTicket(interaction);
			return;
		}

		// Ensure intertactions that get this far are commands
		if (interaction.type !== InteractionType.ApplicationCommand) return;

		// Check that ther command is in our comands collection
		// ingested from 'src/discord/commands'
		const command = bot.commands.get(interaction.commandName);
		if (!command) return;

		// Everything looks good, pull the instructions from the
		// bot's command collection and execute the command.
		try {
			await command.execute(interaction);
		} catch (error) {
			logger.error(error);
			await interaction.reply({ content: strings.errorCommandInteraction, ephemeral: true });
		}
	});

	bot.on('ready', () => {
		logger.info('\x1B[1;97m\x1b[32mClient Ready \x1b[0m');
	});

	bot.on('error', payload => {
		logger.error(payload);
	});

	bot.on('warn', payload => {
		logger.warn(payload);
	});

	bot.on('error', payload => {
		logger.error(payload);
	});

	bot.on('warn', payload => {
		logger.warn(payload);
	});

	bot.login(config.token);
})();
